<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class User_model extends MY_Model
{
    // sets table
    protected $table = 'users';
}

/* End of file client_model.php */
/* Location: ./application/modules/clients/models/client_model.php */