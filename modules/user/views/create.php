<?php if(trim($message) != '') { ?>
      <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $message;?>
      </div>
<?php } ?>
<?php echo form_open("user/create");?>

      <table>
                        <tr>
                              <td style="padding: 10px" valign="top">
                                    <div class="panel panel-default">
                                          <div class="panel-heading">
                                                Identification
                                          </div>
                                          <div class="panel-body">
                                                <label>E-mail</label><br />
                                                <?php echo form_input($email);?>
                                                <br /><br />
                                                <label>Mot de passe</label><br />
                                                <?php echo form_input($password);?>
                                                <br /><br />
                                                <label>Confirmer mot de passe</label><br />
                                                <?php echo form_input($password_confirm);?>                        
                                          </div>
                                    </div> 
                              </td>
                              <td style="padding: 10px" valign="top">
                                    <div class="panel panel-default">
                                          <div class="panel-heading">
                                                Infos personnelles
                                          </div>
                                          <div class="panel-body">     
                                                <label>Prénoms</label><br />
                                                <?php echo form_input($first_name);?>
                                                <br /><br />
                                                <label>nom</label><br />
                                                <?php echo form_input($last_name);?>
                                                <br /><br />
                                                <label>Telephone</label><br />
                                                <?php echo form_input($phone1);?>
                                          </div>
                                    </div>
                              </td>
                              
                        </tr>
                  </table>      
      
      <!-- <p>
            <?php //echo recaptcha_get_html($recaptcha_public_key); ?>
      </p> -->


      <p><input type="submit" name="submit" class="btn btn-primary" value="Cr&eacute;er"></p>

<?php echo form_close();?>