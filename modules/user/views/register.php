<div class="page_title">Comment &ccedil;a marche <span class="small_title"> <span class="bc_separator">></span> Cr&eacute;er <span class="highlighted">mon</span> compte</span></div>

<?php if(trim($message) != '') { ?>
      <div class="info_message"><?php echo $message;?></div>
<?php } ?>
<?php echo form_open("user/register");?>

      <table class="singup">
            <tr>
                  <th>Identification</th>
                  <th>Infos personnelles</th>
                  <th>Conseils</th>
            </tr>
            <tr>
                  <td class="col1">
                        <label>E-mail</label>
                        <?php echo form_input($email);?>               

                        <label>Mot de passe</label>
                        <?php echo form_input($password);?>
                  
                        <label>Confirmer mot de passe</label>
                        <?php echo form_input($password_confirm);?>                        
                  </td>
                  <td class="col2">                        
                        <label>Prénoms</label>
                        <?php echo form_input($first_name);?>

                        <label>nom</label>
                        <?php echo form_input($last_name);?>
                  
                        <label>Telephone</label>
                        <?php echo form_input($phone1);?>                  
                  </td>
                  <td class="col3">
                        <p>1. Utilisez un e-mail valide pour vous inscrire, cette adresse nous permettra de vous envoyer votre confirmation d'inscription
                        ainsi que votre mot de passe en cas d'oubli de celui-ci.</p>
                        <p>2. Vos donn&eacute;es personnelles nous serviront &agrave; communiquer avec vous dans le cadre de l'utilisation de nos services</p>
                  </td>
            </tr>
      </table>      
      
      <p>
            <?php echo recaptcha_get_html($recaptcha_public_key); ?>
      </p>


      <p><?php echo form_submit('submit', 'S\'enregistrer');?></p>

<?php echo form_close();?>