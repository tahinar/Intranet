<?php if($users->num_rows() > 0) { ?>
	<div class="panel panel-default">
        <div class="panel-heading">
            Tous les utilisateurs
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Pr&eacute;noms</th>
                            <th>Nom</th>
                            <th>Nom d'utilisateur</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users->result() as $user) { ?>
                        	<?php $edit_link = site_url() . 'user/profile/' . $user->id; ?>
	                        <tr>
	                            <td><?php echo $user->id ?></td>
	                            <td><?php echo $user->first_name ?></td>
	                            <td><?php echo $user->last_name ?></td>
	                            <td><?php echo $user->username ?></td>
	                            <td><a href="<?php echo $edit_link ?>">Modifier</a></td>
	                        </tr>
	                    <?php } ?>                        
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /panel -->
        
	<div class="pagination"><?php echo $pagination ?></div>

	<br />
	<br />
	<br />

<?php } else { ?>
	<h4>No users</h4>
<?php } ?>