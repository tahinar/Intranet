<html>
<body>
	<h3>Activation de votre compte</h3>
	<p>Bonjour <?php echo $identity;?>,</p>
	<p>Vous venez de cr&eacute;er un compte sur MadAds.com. Pour l'activer, veuillez cliquer sur le lien suivant </p>
	<p><?php echo anchor('auth/activate/'. $id .'/'. $activation, 'Activer mon compte'); ?></p>
	<p>ou</p>
	<p>copier et coller l'URL ci-dessous dans votre barre d'adresse</p>
	<p><?php echo site_url() ?>auth/activate/<?php echo $id . '/' . $activation ?></p>
	<p style="font:normal 10px 'Verdana'; color:#bbb;">_______________<br/>MadAds.com</p>
</body>
</html>