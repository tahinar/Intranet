<?php $i = 1; ?>

<h2>Tous les utilisateurs</h2>

<!-- <p><?php echo lang('index_subheading');?></p> -->

<?php if(trim($message) != '') { ?>
<div class="info_message"><?php echo $message;?></div>
<?php } ?>

<table cellpadding=0 cellspacing=0 width="100%" >
	<tr>
		<th>Prenoms</th>
		<th>Nom</th>
		<th><?php echo lang('index_email_th');?></th>
		<th>Groupe</th>
		<th><?php echo lang('index_status_th');?></th>
		<th><?php echo lang('index_action_th');?></th>
	</tr>
	<?php foreach ($users as $user):?>
		<?php $bg_color = ($i % 2 == 0)? '#fff' : '#f0ecec'; ?>
		<tr bgcolor="<?php echo $bg_color ?>">
			<td><?php echo $user->first_name;?></td>
			<td><?php echo $user->last_name;?></td>
			<td><?php echo $user->email;?></td>
			<td>
				<?php foreach ($user->groups as $group):?>
					<?php echo $group->name; ?><br />
                <?php endforeach?>
			</td>
			<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
			<td><?php echo anchor("auth/edit_user/".$user->id, 'Modifier') ;?></td>
			<!-- <td><a href="edit_user/<?php echo $user->id ?>">Modifier</a></td> -->
		</tr>
		<?php $i++; ?>
	<?php endforeach;?>
</table>

<p><?php echo anchor('auth/create_user', 'Nouvel utilisateur')?> <!-- | <?php echo anchor('auth/create_group', 'Nouveau groupe')?> --></p>