<!DOCTYPE html>
<html lang='fr'>
<head>
    <head>
        <title>Administration</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <!-- css -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo site_url() ?>statics/css/admin/layout.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo site_url() ?>statics/css/admin/style.css" />

        <!-- js -->        
                <script type="text/javascript" src="<?php echo site_url() ?>statics/js/custom.js"></script> 
        <script type="text/javascript" src="<?php echo site_url() ?>statics/js/ckeditor/ckeditor.js"></script> 
                  

        <script language="JavaScript">
            var submitOK = true;
                                       
            function confirmDel(msg) {
                if (confirm(msg)) {
                    submitOK = true; }
                else {
                    submitOK = false;
                }
            }
            
            function chk_del() {
                if (submitOK == false) {
                    submitOK = true;
                    return(false); }
                else {
                    return(true);
                }
            }            
        </script>
        
    </head> 
    <body>
        <div id="header" class="full_width centered">Madads - Administration panel</div>
        <div id="admin_menu" class="full_width centered">
            <ul>
                <li><a href="<?php echo site_url() ?>admin/">Accueil</a></li>
                <li><a href="<?php echo site_url() ?>admin/categories">Cat&eacute;gories d'annonce</a></li>
                <li><a href="<?php echo site_url() ?>admin/advs">Annonces</a></li>
                <li><a href="<?php echo site_url() ?>admin/pages">Pages</a></li>
                <li><a href="<?php echo site_url() ?>admin/frames">Tailles de cadre</a></li>
                <li><a href="<?php echo site_url() ?>admin/">Utilisateurs</a></li>
                <li><a href="<?php echo site_url() ?>auth/logout">Déconnexion</a></li>
            </ul>
        </div>
        <div class="clear_left"></div>
        <div id="content" class="full_width">


<h2>Modifier un utilisateur</h2>

<!-- <p><?php echo lang('edit_user_subheading');?></p> -->

<?php if(trim($message) != '') { ?>
<div class="info_message"><?php echo $message;?></div>
<?php } ?>

<?php echo form_open(uri_string());?>
      <table>
      <tr>
            <td class="bold">Pr&eacute;noms</td>
            <td><?php echo form_input($first_name);?></td>
      </tr>

      <tr>
            <td class="bold">Nom</td>
            <td><?php echo form_input($last_name);?></td>
      </tr>
      <tr>
            <td class="bold">T&eacute;l&eacute;phone</td>
            <td><?php echo form_input($phone1);?></td>
      </tr>

      <tr>
            <td class="bold">Mot de passe (si modification)</td>
            <td><?php echo form_input($password);?></td>
      </tr>

      <tr>
            <td class="bold">Confirmation mot de passe (si modification)</td>
            <td><?php echo form_input($password_confirm);?></td>
      </tr>
      
      <?php if($is_admin) { ?>
      <tr>
      	<td class="bold">Groupe</td>
      	<td>
                  <?php foreach ($groups as $group):?>
            	<label class="checkbox">
            	<?php
            		$gID=$group['id'];
            		$checked = null;
            		$item = null;
            		foreach($currentGroups as $grp) {
            			if ($gID == $grp->id) {
            				$checked= ' checked="checked"';
            			break;
            			}
            		}
            	?>
            	<input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
            	<?php echo $group['name'];?>
            	</label>
            
      	<?php endforeach?>
            </td>
      </tr>     
      <?php } // for admin only ?>

      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>

      <tr><td colspan="2"><?php echo form_submit('submit', 'Enregistrer');?></td></tr>

</table>
<?php echo form_close();?>

</div>
        <div id="footer" class="full_width centered">&copy; Madads - All rights reserved 2013</div>
    </body> 
</html>