<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Ged_model extends MY_Model
{
    // sets table
    protected $table = 'documents';
    protected $documentTypeTable = 'document_types';
    protected $assDocumentTypeTable = 'ass_type_document';

    /**
     * Search for companies
     *
     * This function retrieves a series of db entries based on criteria.
     *
     * @param string $like Mostly name or siret of the document
     * @param   array   $criteria       A keyed array of criteria key = field name, value = value, key may also contain comparators (=, !=, >, etc..)
     * @param   int     $limit          The max number of entries to grab (0 = no limit)
     * @param   int     $offset         What record number to start grabbing (useful for pagination)
     * @param   array   $order          A keyed array of "order commands" telling how to sort key = field name, value = direction (asc, desc, random)
     * @param   array   $order          A keyed array of like key = field name
     * @access  public
     * @return  mixed   Return Object of results on success, Boolean False on failure
     */
    public function search($fields = "*", $criteria = '', $limit = 0, $offset = 0, $order = '') 
    {
        
        // verify the table we are drawing from has been set.
        if (!empty($this->table)) 
        {
            $this->db->from($this->table);
        } 
        else 
        {
            // If the table has not been set... we cannot
            log_message('error', 'Model: Crud; Method: retrieve($criteria, $limit, $offset, $order); Required to select_table before using functions.');
            return FALSE;
        }


        // Native sql string
        $sqlString = "SELECT " . $fields . " FROM " . $this->table;

        // Joins table if necessary
        if(isset($criteria['has_phone']) || isset($criteria['has_email']) || isset($criteria['has_fax']) || isset($criteria['has_website']))
        {
            $sqlString .= " LEFT JOIN document_infos ON companies.SIRET = document_infos.SIRET ";
        }

        $sqlString .= " WHERE 1=1 ";
        
        // Verify an array has been passed.
        if (is_array($criteria)) 
        {

            // Name or siret : NOMEN_LONG, SIRET
            if(isset($criteria['nameAndSiret']) && strlen(isset($criteria['nameAndSiret'])) > 0)
            {
                $sqlString .= " AND (NOMEN_LONG LIKE '%" . $this->db->escape_like_str($criteria['nameAndSiret']) . "%' ESCAPE '!'";
                $sqlString .= " OR SIRET LIKE '%" . $this->db->escape_like_str($criteria['nameAndSiret']) ."%' ESCAPE '!') ";
            }
            
            if (is_array($order)) 
            {
                foreach ($order as $order_by => $direction) 
                {
                    $sqlString .= " ORDER BY " . $order_by . " " . $direction;
                    //$this->db->order_by($order_by, $direction);
                }
                unset($order_by, $direction);                
            }

            if (!empty($limit)) 
            {
                if (!empty($offset)) 
                {
                    $sqlString .= " LIMIT " . $offset . ", " . $limit;
                    //$this->db->limit($limit, $offset);
                } 
                else 
                {
                    $sqlString .= " LIMIT " . $limit;
                    //$this->db->limit($limit);
                }
            }
            //echo $sqlString;
            $res = $this->db->query($sqlString);
            //$res = $this->db->get();
            //echo $this->db->get_compiled_select();
            //echo $this->db->last_query();
            return $res;
        } 
        else 
        {
            log_message('error', 'Model: Crud; Method: retrieve($criteria, $limit, $offset, $order); Required parameter not set.');
            return FALSE;
        }
    }

    /**
     * count_results
     *
     * This function simply counts ALL entries in the selected DB with a given criteria.
     *
     * @param   array   $criteria    A keyed array with the critera for selecting what entries to edit.
     * @access  public
     * @return  Mixed   Return Integer of resutls on success, Boolean False on failure
     */
    public function count_search_results($fields = 'SIRET', $criteria) 
    {
        // verify the table we are drawing from has been set.
        if (empty($this->table)) 
        {
            // If the table has not been set... we cannot
            log_message('error', 'Model: Crud; Method: count_results($criteria); Required to select_table before using functions.');
            return FALSE;
        }
        
        // Native sql string
        $sqlString = "SELECT companies.SIRET FROM " . $this->table;

        // Joins table if necessary
        if(isset($criteria['has_phone']) || isset($criteria['has_email']) || isset($criteria['has_fax']) || isset($criteria['has_website']))
        {
            $sqlString .= " LEFT JOIN document_infos ON companies.SIRET = document_infos.SIRET ";
        }

        $sqlString .= " WHERE 1=1 ";

        // Name or siret : NOMEN_LONG, SIRET
        if(isset($criteria['nameAndSiret']) && strlen(isset($criteria['nameAndSiret'])) > 0)
        {
            $sqlString .= " AND (NOMEN_LONG LIKE '%" . $this->db->escape_like_str($criteria['nameAndSiret']) . "%' ESCAPE '!'";
            $sqlString .= " OR SIRET LIKE '%" . $this->db->escape_like_str($criteria['nameAndSiret']) ."%' ESCAPE '!') ";
        }
     
        return $this->db->query($sqlString)->num_rows();
        //return $this->db->get($this->table)->num_rows();
        //return $this->db->count_all_results();
    }

    /**
     * Get single document contact details
     *
     * retrives 1 row from document's infos depending on condition in $where
     *
     * @access  public
     * @param   array $where WHERE filed_name => field_value
     * @access  public
     * @return  Mixed   Return Integer of resutls on success, Boolean False on failure
     */
    public function _get_document_contact_details($where = array())
    {
        $fields = '*';
        
        $this->db->select($fields);
        $this->db->from($this->documentInfoTable);

        if(!is_array($where))
        {
            $where = array(
                    'id' => intval($where) // forces query to look for id
                );
        }
        

        $this->db->where($where);            
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            $result = $query->row_object();            
            return $result;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Update document's details
     *  
     * @param   array    $criterea  WHERE
     * @param   array    $escaped_data       A keyed array of criteria key = field name, value = value for escaped data
     * @param   array    $non_escaped_data   A keyed array of criteria key = field name, value = value for non escaped data
     * @access  public
     * @return  bool     FASLE | TRUE on success or failure
     */
    public function update_document_details($data = array())
    {

        if (is_array($data)) 
        {
            return $this->db->replace($this->documentInfoTable, $data);
        }
        else 
        {
            log_message('error', 'Model: Crud; Method: update ($date); Required parameter not set.');
            return FALSE;
        }
    }

    /**
     * List document types
     *  
     * @param   array    $criterea  WHERE
     * @param   array    $escaped_data       A keyed array of criteria key = field name, value = value for escaped data
     * @param   array    $non_escaped_data   A keyed array of criteria key = field name, value = value for non escaped data
     * @access  public
     * @return  bool     FASLE | TRUE on success or failure
     */
    function _list_document_types ($fields = '*')
    {

        // verify the table we are drawing from has been set.
        if (!empty($this->documentTypeTable)) 
        {
            $this->db->from($this->documentTypeTable);
        } 
        else 
        {
            // If the table has not been set... we cannot
            log_message('error', 'Model: Crud; Method: _list_document_types(); Required to select_table before using functions.');
            return FALSE;
        }

        // Native sql string
        $sqlString = "SELECT " . $fields . " FROM " . $this->documentTypeTable;
        return $this->db->query($sqlString);
    }

    /**
     * Gets document types
     *
     * @param int $id document id
     * return array document types
     */
    function _get_document_types($id){
        
        $query = "SELECT document_type FROM " . $this->assDocumentTypeTable;
        $query .= " LEFT JOIN " . $this->documentTypeTable . " ON ";
        $query .= $this->documentTypeTable . ".id = " . $this->assDocumentTypeTable . ".document_type_id ";
        $query .= " WHERE " . $this->assDocumentTypeTable . ".document_id = " . $id;
       
        $res = $this->db->query($query);
        
        return $res;
    }
}

/* End of file Ged_model.php */
/* Location: ./application/modules/ged/models/Ged_model.php */