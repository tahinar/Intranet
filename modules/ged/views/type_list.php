<?php if($document_types->num_rows() > 0) { ?>
	<ul class="document_type_list">
		<?php foreach ($document_types->result() as $types) { ?>
			<li class="col-md-3">
				<em class="fa fa-fw fa-folder-open"></em> 
				<a href="<?php echo site_url() ?>ged/type/<?php echo $types->id ?>"><?php echo $types->document_type ?></a></li>
		<?php } ?>
	</ul>
<?php } else { ?>
	No document types found
<?php } ?>