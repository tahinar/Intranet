<?php 
$i = 1;
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    GED
    <small>Liste de tous les documents</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
    <li><a href="#">GED</a></li>
    <li class="active">Documents</li>
  </ol>
</section>

<section class="content">

	<?php if($documents->num_rows() > 0) { ?>
		<div class="box">
            <div class="box-header">
              <h3 class="box-title">Tous les documents</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th width="0%">#</th>
                  <th width="80%">Titre</th>
                  <th width="10%">Type</th>
                  <th width="5%">Ajout</th>
                  <th width="5%">Modif.</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($documents->result() as $document) { ?>
	                <tr>
	                  <td><?php echo $i; ?></td>
	                  <td><a href="<?php echo site_url() ?>ged/edit/<?php echo $document->id ?>"><?php echo $document->title ?></a>
	                  </td>
	                  <td>
	                  	<?php show_document_types($document->id); ?>
	                  	</td>
	                  <td><?php echo my_format_date('form', $document->add_date) ?></td>
	                  <td><?php echo my_format_date('form', $document->update_date) ?></td>
	                </tr>
	                <?php $i++; ?>
                <?php } ?>
                </tfoot>
              </table>
              <ul class="pagination">
			        <?php echo $pagination ?>
			    </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
		
	<?php } else { ?>
		No document found
	<?php } ?>
</section>