<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ged extends MX_Controller {

	protected $table = 'documents';
	
	public function __construct()
    {
      parent::__construct();
        
  		$this->load->library('layout');
      $this->layout->set_theme('intranet');
      $this->load->helper('date_format_helper');
      $this->load->helper('document_type_helper');
      $this->load->model('ged_model', 'gedManager');

      $redirect = redirect_url(TRUE);

      if (!$this->ion_auth->logged_in())
      {
          //redirect them to the home ad because they must be an administrator to view this
          redirect('auth/login?back=' . $redirect, 'refresh');            
      }
    }


	/**
	 * Index Page for this controller.	 *
	 */
	public function index()
	{
		redirect('ged/all', 'refresh');
	}


  /**
   * list companies
   *      
   * Retrieves the list of ads. Displays list for index and list_ads
   *      
   * @access public
   * @param int $page The page number (pagination)
   * @param session $user->id The id of the current logged in user
   * @return resutlset
   *
   */
  public function all($page = 0)
  {  
    // vars
    $limit = 50; // items per page
    $like = "";
    $data['controller'] = $this;
    
    // pagination    
    $this->load->library('pagination');

    // calculates offset
    $page = (($page - 1) >=0 )? ($page - 1) : 0;
    $offset = $page * $limit;
    
    $fields = "documents.id, documents.title, documents.add_date, documents.update_date";

    // Get the list of Items from the DB
    $criteria = array('1' => 1);    // Any entry that has an ID not equal to zero. 
    //if(!$this->ion_auth->is_admin()) $criteria['user_id'] = $user->id;    // Shows only the ads of the current user
    
    $order_by = array('id' => 'DESC');   // order by id asscending
    $query = $this->gedManager->retrieve($fields, $criteria, $limit, $offset, $order_by, $like);

    // pagination
    $this->load->library('pagination');
    $config['base_url']   = site_url() . 'ged/all';
    $config['total_rows'] = $this->gedManager->count_results($fields, $criteria, $like);
    $config['per_page']   = $limit;    // if you change this you must also change the crud call below.        
    $config['use_page_numbers'] = TRUE;
    $config['first_link'] = '&laquo;';
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['prev_link'] = '&lt; Prec';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['next_link'] = 'Suiv &gt;';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $config['last_link'] = '&raquo;';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    $this->pagination->initialize($config); 
    $data['pagination'] = $this->pagination->create_links();

    $data['documents'] = $query;
    $data['total_documents'] = $config['total_rows'];
    $data['countStart'] = $offset + 1; // We start at 1
    $data['is_search'] = FALSE;
    
    // load layout
    //$this->layout->set_theme($root_theme . 'default');
    $this->layout->set_titre('GED - Toutes les documents');
    //$this->layout->add_css('layout');
    //$this->layout->add_css('style');
    //$this->layout->add_js('jquery-1.8.3');
    $this->layout->view('ged/document_list', $data);
  }

  /**
   * Lists all document types
   */
  public function type_list()
  {
    $data['section_title'] = 'GED';  

    //loads layout
    $this->layout->set_titre("GED");
    $this->layout->view('ged/type_list', $data);
  }

  /**
   * Embed list document types
   */
  public function embed_type_list()
  {
    $resTypes = $this->gedManager->_list_document_types();
    $data['document_types'] = $resTypes;
    $this->load->view('ged/type_list', $data);
  }  
	
  /**
   * Edit a document
   *      
   * @access public
   * @param int $id The document id
   * @return resutlset
   *
   */
  public function edit($id)
  {
    $this->load->library('form_validation');

    $where = array(
      'id' => $id
    );

    $where_type = array(
      'document_id' => $id
    );

    // Get document
    $result = $this->gedManager->get_single($where);
    // Get document type
    $resultDetails = $this->gedManager->_get_document_types($id);
    
    if($result) // if company exists
    {
      $data['companyExists'] = TRUE;
      $data['document'] = $result; 
    }
    else
    {
      $data['companyExists'] = FALSE; 
    }
    
    //$this->layout->set_theme('modal');
    $this->layout->set_titre("Modifier");
    $this->layout->view('ged/edit', $data);   
  }
}  