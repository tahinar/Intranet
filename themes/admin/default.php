<!DOCTYPE html>
<html lang='fr'>
<head>
    <head>
        <title><?php echo $title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $charset; ?>" />
        <!-- css -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo site_url() ?>statics/css/admin/layout.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo site_url() ?>statics/css/admin/style.css" />

        <!-- js -->        
        <?php foreach($js as $js_url): ?>
            <script type="text/javascript" src="<?php echo $js_url; ?>"></script> 
        <?php endforeach; ?>
        <script type="text/javascript" src="<?php echo site_url() ?>statics/js/custom.js"></script> 
        <script type="text/javascript" src="<?php echo site_url() ?>statics/js/ckeditor/ckeditor.js"></script> 
	       	

        <script language="JavaScript">
            var submitOK = true;
                                       
            function confirmDel(msg) {
                if (confirm(msg)) {
                    submitOK = true; }
                else {
                    submitOK = false;
                }
            }
            
            function chk_del() {
                if (submitOK == false) {
                    submitOK = true;
                    return(false); }
                else {
                    return(true);
                }
            }            
        </script>
        
    </head> 
    <body>
        <div id="header" class="full_width centered">Madads - Administration panel</div>
        <div id="admin_menu" class="full_width centered">
            <ul>
                <li><a href="<?php echo site_url() ?>admin/">Accueil</a></li>
                <li><a href="<?php echo site_url() ?>admin/categories">Cat&eacute;gories d'annonce</a></li>
                <li><a href="<?php echo site_url() ?>admin/advs">Annonces</a></li>
                <li><a href="<?php echo site_url() ?>admin/pages">Pages</a></li>
                <li><a href="<?php echo site_url() ?>admin/frames">Tailles de cadre</a></li>
                <li><a href="<?php echo site_url() ?>admin/">Utilisateurs</a></li>
                <li><a href="<?php echo site_url() ?>admin/mail_account/">Comptes mail</a></li>
                <li><a href="<?php echo site_url() ?>auth/logout">Déconnexion</a></li>
            </ul>
        </div>
        <div class="clear_left"></div>
        <div id="content" class="full_width">
            <?php echo $output; ?>
        </div>
        <div id="footer" class="full_width centered">&copy; Madads - All rights reserved 2013</div>
    </body> 
</html>