<?php $is_home = (isset($is_home))? $is_home : FALSE; ?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="<?php echo $charset; ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo site_url() ?>statics/sb2/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo site_url() ?>statics/sb2/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo site_url() ?>statics/sb2/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo site_url() ?>statics/sb2/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>

    <div id="wrapper">
        <?php echo $output; ?>         
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo site_url() ?>statics/sb2/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo site_url() ?>statics/sb2/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo site_url() ?>statics/sb2/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <!-- <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo site_url() ?>statics/sb2/dist/js/sb-admin-2.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            var successMessage = '';
            successMessage += '<div class="alert alert-success alert-dismissable">';
            successMessage += '    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
            successMessage += '    Données enregistrées.'
            successMessage += '</div>';
            
            var failureMessage = '';
            failureMessage += '<div class="alert alert-danger alert-dismissable">';
            failureMessage += '   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
            failureMessage += '    Erreur lors de linsertion.';
            failureMessage += '</div>';
                        
            var frm = $('#update_details_form')

            frm.submit(function(e) {
                //alert($modal.modal());
                $.ajax({
                        type: "POST",
                        url: frm.attr('action'),
                        data: frm.serialize(), 
                        success: function(data)
                        {
                            //alert(data); // show response from the php script.
                            $htmlData = (data == 'Success')? successMessage : failureMessage ;
                            $('#ajax_message').html($htmlData);
                            setTimeout(function(){ 
                                $('.close').trigger('click');
                                // $('.modal-backdrop').remove();
                                // $('#ajaxModal').remove();
                            }, 1500);         
                        }
                    });
                
                e.preventDefault(); // avoid to execute the actual submit of the form.                
            });
        });
    </script>

</body>

</html>