<?php $is_home = (isset($is_home))? $is_home : FALSE; ?>

<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="<?php echo $charset; ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo site_url() ?>statics/sb2/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo site_url() ?>statics/sb2/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo site_url() ?>statics/sb2/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo site_url() ?>statics/sb2/data-cm.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo site_url() ?>statics/sb2/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url() ?>">Data - Cutter Marketing</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-file-text fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="#"><i class="fa fa-user fa-fw"></i> Profil</a> -->
                        </li>
                        <li><a href="<?php echo site_url() ?>companies/import"><i class="fa fa-plus fa-fw"></i> Importer des sociétés</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url() ?>companies/all"><i class="fa fa-th-list fa-fw"></i> Voir toutes les sociétés</a>
                        </li>                        
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user-md fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="#"><i class="fa fa-user fa-fw"></i> Profil</a> -->
                        </li>
                        <li><a href="<?php echo site_url() ?>user/create"><i class="fa fa-plus fa-fw"></i> Ajouter un utilisateur</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url() ?>user/all"><i class="fa fa-th-list fa-fw"></i> Voir tous les utilisateurs</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-gear fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="#"><i class="fa fa-user fa-fw"></i> Profil</a> -->
                        </li>
                        <li><a href="<?php echo site_url() ?>user/profile"><i class="fa fa-gear fa-fw"></i> Mon compte</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url() ?>auth/logout"><i class="fa fa-sign-out fa-fw"></i> Se d&eacute;connecter</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <!-- <ul class="nav" id="side-menu">
                        <li class="sidebar-search"> -->
                            <!-- <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Rechercher...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div> -->
                            <!-- /input-group -->
                        <!-- </li> -->
                        <!-- <li>
                            <a href="index.html"><i class="fa fa-gear fa-fw"></i> Filtrer les recherche par</a>
                        </li> -->
                        <!-- <li> -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Outil de recherche
                                </div>
                                <div class="panel-body">
                                    <form name="search" method="get" action="<?php echo site_url() ?>companies/search">
                                        <input type="text" name="s" class="form-control" placeholder="Nom /Raison social/Siret">
                                        <br />
                                        <label>APET</label>
                                        <br />
                                        <?php $apet = (isset($searchTerms['apet']))? $searchTerms['apet'] : ''; ?>
                                        <input type="text" name="apet" class="form-control" placeholder="N&deg; ou libell&eacute;" value="<?php echo $apet; ?>">
                                        <br />
                                        <label>Localisation </label>
                                        <br />
                                        <?php $location = (isset($searchTerms['location']))? $searchTerms['location'] : ''; ?>
                                        <input type="text" name="location" class="form-control" placeholder="Région/Département/Ville" value="<?php echo $location; ?>">
                                        <br />
                                        <!-- <label>Chiffre d'affaire </label>
                                        <br />
                                        <?php $operatingIncome = (isset($searchTerms['operating_income']))? $searchTerms['operating_income'] : ''; ?>
                                        <select class="form-control" name="operating_income">
                                            <option value="">Choisir</option>
                                            <option <?php echo ($operatingIncome == '0')? 'selected=selected' : ''; ?> value="0">CA < 0,5M</option>
                                            <option <?php echo ($operatingIncome == '1')? 'selected=selected' : ''; ?> value="1">0,5M < CA < 1M</option>
                                            <option <?php echo ($operatingIncome == '2')? 'selected=selected' : ''; ?> value="2">1M < CA < 2M</option>    
                                            <option <?php echo ($operatingIncome == '3')? 'selected=selected' : ''; ?> value="3">2M < CA < 5M</option>
                                            <option <?php echo ($operatingIncome == '4')? 'selected=selected' : ''; ?> value="4">5M < CA < 10M</option>
                                            <option <?php echo ($operatingIncome == '5')? 'selected=selected' : ''; ?> value="5">10M < CA < 20M</option>
                                            <option <?php echo ($operatingIncome == '6')? 'selected=selected' : ''; ?> value="6">20M < CA < 50M</option>
                                            <option <?php echo ($operatingIncome == '7')? 'selected=selected' : ''; ?> value="7">50M < CA < 100M</option>
                                            <option <?php echo ($operatingIncome == '8')? 'selected=selected' : ''; ?> value="8">100M < CA < 200M</option>
                                            <option <?php echo ($operatingIncome == '9')? 'selected=selected' : ''; ?> value="9">CA > 200M</option>
                                        </select>
                                        <br /> -->
                                        <label>Surface </label>
                                        <br />
                                        <?php $surface = (isset($searchTerms['surface']))? $searchTerms['surface'] : ''; ?>
                                        <select class="form-control" name="surface">
                                            <option value="">Choisir</option>
                                            <option <?php echo ($surface == '1')? 'selected=selected' : ''; ?> value="1">Moins de 300 m²</option>
                                            <option <?php echo ($surface == '2')? 'selected=selected' : ''; ?> value="2">De 300 m² à moins de 400 m²</option>    
                                            <option <?php echo ($surface == '3')? 'selected=selected' : ''; ?> value="3">De 400 m² à moins de 2 500 m²</option>
                                            <option <?php echo ($surface == '4')? 'selected=selected' : ''; ?> value="4">2 500 m² ou plus</option>
                                                                                        
                                        </select>
                                        <br />
                                        <label>Effectif </label>
                                        <br />
                                        <?php $staffNumber = (isset($searchTerms['staff_nbr']))? $searchTerms['staff_nbr'] : '0-0'; ?>
                                        <select class="form-control" name="staff_nbr">
                                            <option <?php echo ($staffNumber == '0-0')? 'selected=selected' : ''; ?> value="0-0">Choisir</option>
                                            <option <?php echo ($staffNumber == '0-10')? 'selected=selected' : ''; ?> value="0-10">0 - 10</option>
                                            <option <?php echo ($staffNumber == '11-50')? 'selected=selected' : ''; ?> value="11-50">11 - 50</option>    
                                            <option <?php echo ($staffNumber == '51-100')? 'selected=selected' : ''; ?> value="51-100">51 - 100</option>
                                            <option <?php echo ($staffNumber == '101-200')? 'selected=selected' : ''; ?> value="101-200">101 - 200</option>
                                            <option <?php echo ($staffNumber == '201-300')? 'selected=selected' : ''; ?> value="200-300">201 - 300</option>
                                            <option <?php echo ($staffNumber == '301-0')? 'selected=selected' : ''; ?> value="301-0">300+</option>
                                        </select>
                                        
                                        <br /><br />

                                        <p><b>Qui ont :</b></p>
                                        <input <?php echo (isset($_GET['has_email']))? 'checked=checked' : ''; ?> type="checkbox" name="has_email" /> E-mail <br />
                                        <input <?php echo (isset($_GET['has_phone']))? 'checked=checked' : ''; ?> type="checkbox" name="has_phone" /> Téléphone <br />
                                        <input <?php echo (isset($_GET['has_fax']))? 'checked=checked' : ''; ?> type="checkbox" name="has_fax" /> Fax <br />
                                        <input <?php echo (isset($_GET['has_website']))? 'checked=checked' : ''; ?> type="checkbox" name="has_website" /> Site web <br />
                                        <br /><br />
                                        <input type="submit" class="btn btn-primary" value="Chercher">
                                    </form>
                                </div>
                                <!-- <div class="panel-footer">
                                    Panel Footer
                                </div> -->
                            </div>
                            
                        <!-- </li>
                        
                    </ul> -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?php echo $title; ?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <?php echo $output; ?>
            <!-- <div class="row">
                <div class="col-lg-8">
                    
                    
                </div> -->
                <!-- /.col-lg-8 -->
                <!-- <div class="col-lg-4">
                    
                </div> -->
                <!-- /.col-lg-4 -->
            <!-- </div> -->
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo site_url() ?>statics/sb2/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo site_url() ?>statics/sb2/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo site_url() ?>statics/sb2/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <!-- <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo site_url() ?>statics/sb2/dist/js/sb-admin-2.js"></script>

    <script type="text/javascript">
        $('[data-toggle="ajaxModal"]').on('click',
            function(e) {
                $('#ajaxModal').remove();
                e.preventDefault();
                var $this = $(this)
                , $remote = $this.data('remote') || $this.attr('href')
                , $modal = $('<div class="modal" id="ajaxModal"><div class="modal-body"></div></div>');
                $('body').append($modal);
                $modal.modal({backdrop: 'static', keyboard: false});
                $modal.load($remote);
            }
        );
        // $('#ajaxModal').on('hidden.bs.modal', function (e) {
        //     $('#ajaxModal').remove(); // Remove from DOM.
        // });
        // $('#myModal').on('show.bs.modal', function (e) {
        //     e.preventDefault();
        //     //$modal.load('http://localhost/datamarketing/companies/view/40996826000037');
        //     //e.preventDefault();
        // });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            
            var badge_class = '';
            var badge_ico = '';
            //alert('ao');
            
            $( "img.company-status" ).each( function( index, listItem ) {
 
                var siret;
                var statusUrl;
                var badge_class;
                var badge_ico;
                var $this = $(this);
                
                siret = $(this).attr('data-siret');
                
                statusUrl = '<?php echo site_url() ?>companies/status/' + siret;
                                    
                $.ajax({
                   type: "POST",
                   url: statusUrl,
                   data: '', 
                   success: function(data)
                   {
                       //alert(data);
                       switch(data) {
                            case 'Complete':
                                // badge_class = 'success';
                                badge_ico = '<?php echo site_url() ?>statics/sb2/images/complete.png';
                            break;

                            case 'Incomplete':
                                // badge_class = 'warning';
                                badge_ico = '<?php echo site_url() ?>statics/sb2/images/incomplete.png';
                            break;

                            case 'None':
                                // badge_class = 'danger';
                                badge_ico = '<?php echo site_url() ?>statics/sb2/images/none.png';
                            break;

                            default:
                                // badge_class = 'danger';
                                badge_ico = '<?php echo site_url() ?>statics/sb2/images/none.png';
                       }
                       $this.attr('src', badge_ico);
                       //$this.addClass('badge-' + badge_class);
                       //$this.children().first().addClass('fa-' + badge_ico);
                    }
                }); 
            }); // each

            /* Company infos */
            $( ".company-infos-icons" ).each( function( index, listItem ) {
                var siret;
                var statusUrl;
                var $this = $(this);

                siret = $(this).attr('data-siret');
                                
                statusUrl = '<?php echo site_url() ?>companies/features/' + siret;

                $.ajax({
                   type: "POST",
                   url: statusUrl,
                   data: '', 
                   success: function(data)
                   {                       
                       $this.html(data);                       
                    }
                }); 
            }); // each feat
        });
    </script>

</body>

</html>
