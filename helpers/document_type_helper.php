<?php

/**
* Shows document types by document ID
*/
function show_document_types($id)
{
	// Get a reference to the controller object
    $CI = get_instance();

    // You may need to load the model if it hasn't been pre-loaded
    $CI->load->model('Ged_model', 'gedManager');

	$arrTypes = array();

	$resTypes = $CI->gedManager->_get_document_types($id);
	foreach ($resTypes->result() as $types) {
		$arrTypes[] = $types->document_type;
	}

	echo join($arrTypes, ', ');
}