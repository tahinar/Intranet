<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if ( ! function_exists('site_url'))
{
    function site_url($uri = '')
    {       
        if( ! is_array($uri))
        {
            //  Tous les paramètres sont insérés dans un tableau
            $uri = func_get_args();
        }
     
        //  On ne modifie rien ici
        $CI =& get_instance();  
        return $CI->config->site_url($uri);
    }
}
 
// ------------------------------------------------------------------------
 
if ( ! function_exists('url'))
{
    function url($text, $uri = '')
    {
        if( ! is_array($uri))
        {
            //  Suppression de la variable $text
            $uri = func_get_args();
            array_shift($uri);
        }
     
        echo '<a href="' . site_url($uri) . '">' . htmlentities($text) . '</a>';
        return '';
    }
}

/*
Retrieves $_GET['back'] from current url and returns it
*/
if ( ! function_exists('redirect_url'))
{
    function redirect_url($is_admin = FALSE)
    {
        if($is_admin) 
        {
            /* admin */
            $current_url = uri_string();
            $redirect = $current_url;
        } else {
            /* front-end */
            $s = 1;
            $params = array();
            $current_url = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
            $url_arr = explode('?', $current_url);
            //echo uri_string();
            foreach ($url_arr as $key => $value) {
                if($s > 1) $params[] = $value;
                $s++;
            }

            $redirect = join('?', $params);
            $redirect = str_replace('back=', '', $redirect);
        }

        return $redirect;
    }
}


/**
 * Checks and formats URL: checks http:// or https:// in a URL. Adds them if necessary
 */
if ( ! function_exists('check_and_format_URL'))
{
    function check_and_format_URL($url)
    {
        if(trim($url) != '') {
            if ((strpos($url, 'http://') === FALSE) || (strpos($url, 'https://') === FALSE)) {
                $url = 'http://' . $url;
            }
        }
        
        return $url;        
    }
}


/**
 * Adds params to current url
 * @param array $params An array of parameters key->value
 */
if ( ! function_exists('add_url_params'))
{
    function add_url_params($params)
    {
        $counter = 0;
        $protocol = (isset($_SERVER['HTTPS']))? 'https://': 'http://';
        $currentLink = rtrim(site_url(), '/') . $_SERVER['REQUEST_URI']; // online
        //$currentLink = 'http://localhost' . $_SERVER['REQUEST_URI'];

        // get file/controller
        $arrURI = explode('?', $currentLink);
        $fileController = (isset($arrURI[0]))? $arrURI[0] : site_url();
        $urlParams = $fileController;

        // Clean URL in case we already have a key>value pair corresponding to params
        foreach($_GET as $key=>$value) {
            // skip param if it exists
            if(!array_key_exists($key, $params)) {
                $urlParams .= ($counter == 0)? '?' : '&' ;
                $urlParams .= $key . "=" . $value;
            }
            $counter++;
        }

        $urlBack = $urlParams;
        // Get key>value of params
        foreach($params as $key=>$value) {
            if(strstr($urlBack, '?')) {
                $urlBack .= '&amp;' . $key . '=' . $value;
            } else {
                $urlBack .= '?' . $key . '=' . $value;
            }            
        }
                
        return $urlBack;
    }
}

/**
 * Rebuilds GET params
 * 
 */
if ( ! function_exists('rebuild_get_params'))
{
    function rebuild_get_params()
    {
        $newUrlParams = '';

        if(isset($_GET))
        {
            foreach($_GET as $key => $value)
            {
                $newUrlParams .= (strlen(trim($newUrlParams)) > 0)? '&amp;': '?';
                $newUrlParams .= $key . '=' . $value;
            }
        }

        return $newUrlParams;
    }
}
 
/* End of file MY_url_helper.php */
/* Location: ./application/helpers/MY_url_helper.php */