<?php 

if ( !function_exists('get_operating_income_label'))
{
    /*
    Gets operating income labels
    */
    function get_operating_income_label($key = 0)
    {
        $arrOperatingIncomeLabels = array (
        	"Moins de 0,5 million d'euros",
        	"De 0,5 à moins de 1 million d'euros",
        	"De 1 million à moins de 2 millions d'euros",
        	"De 2 millions à moins de 5 millions d'euros",
    		"De 5 millions à moins de 10 millions d' euros",
    		"De 10 millions à moins de 20 millions d'euros",
    		"De 20 millions à moins de 50 millions d'euros",
    		"De 50 millions à moins de 100 millions d'euros",
    		"De 100 millions à moins de 200 millions d'euros",
    		"200 millions d'euros ou plus"		
        );

        return $arrOperatingIncomeLabels[intval($key)];
    }
}

if ( !function_exists('get_order_by_field'))
{
    /*
    Gets table field names
    */
    function get_order_by_field($key = 0)
    {
        $fieldName = 'NOMEN_LONG';
        $arrOrderLabels = array (
            'oi' => 'TCA',
            'name' => 'NOMEN_LONG'      
        );

        $fieldName = (isset($arrOrderLabels[$key]))? $arrOrderLabels[$key] : 'NOMEN_LONG';

        return $fieldName;
    }
}

if ( !function_exists('surface_label'))
{
    /*
    Gets table field names
    */
    function surface_label($key = 0)
    {
        $arrSurface = array(
            "N/A",
            "Moins de 300 m²",
            "De 300 m² à moins de 400 m²",
            "De 400 m² à moins de 2 500 m²",
            "2 500 m² ou plus"
        );       

        $surfaceLabel = (isset($arrSurface[$key]))? $arrSurface[$key] : 'N/A';

        return $surfaceLabel;
    }
}

if ( !function_exists('get_activity_detail_label'))
{
    /*
    Gets table field names
    */
    function get_activity_detail_label($key = 0)
    {
        $arrActivityLabels = array(
            '03' => 'Extraction',
            '04' => 'Fabrication, production',
            '05' => 'Montage, installation',
            '06' => 'Réparation',
            '07' => 'Transport',
            '08' => 'Import, export',
            '09' => 'Commerce de gros ou intermédiaire du commerce',
            '10' => 'Commerce de détail',
            '11' => 'Profession libérale',
            '12' => 'Services',
            '13' => 'Location de meublés',
            '14' => 'Bâtiments, travaux publics',
            '15' => 'Services aux entreprises',
            '20' => 'Donneur d\'ordre',
            '99' => 'Autre',
            'NR' => 'Non renseigné'
        );       

        $activityLabel = (isset($arrActivityLabels[$key]))? $arrActivityLabels[$key] : 'N/A';

        return $activityLabel;
    }
}