<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('image_exists'))
{
	function image_exists($image) 
	{
	    $tmpImg = @getimagesize($image);
	    if(is_array($tmpImg)) return true;
	    else return false;
	}
}